import { default as chai, expect } from 'chai'
import sinon from 'sinon'

import { default as JwtRbac, permission } from '../lib/JwtRbac'

const jwtRbac = new JwtRbac("URL", "AUDIENCE", "PUBLIC_KEY")
const tenantId = "TENANT_ID"
const service = "SERVICE"
const resource = "RESOURCE"
const jwt = "JWT"

describe('JwtRbac', () => {
    it('should have url', async () => {
        expect(jwtRbac.padlockUrl).to.equal("URL")
    })

    it('should have audience', async () => {
        expect(jwtRbac.audience).to.equal("AUDIENCE")
    })
    
    it('should have public key', async () => {
        expect(jwtRbac.publicKey).to.equal("PUBLIC_KEY")
    })

    it('should return hasPermissions result from canCreate', async () => {
        const hasPermissionsStub = sinon.stub(jwtRbac, 'hasPermissions').returns(true)

        const result = jwtRbac.canCreate(tenantId, service, resource, jwt)

        jwtRbac.hasPermissions.restore()

        expect(result).to.be.true
        expect(hasPermissionsStub.calledOnce)
        expect(hasPermissionsStub.calledWith(tenantId, service, resource, [permission.CREATE], jwt))
    })

    it('should return hasPermissions result from canRead', async () => {
        const hasPermissionsStub = sinon.stub(jwtRbac, 'hasPermissions').returns(true)

        const result = jwtRbac.canRead(tenantId, service, resource, jwt)

        jwtRbac.hasPermissions.restore()

        expect(result).to.be.true
        expect(hasPermissionsStub.calledOnce)
        expect(hasPermissionsStub.calledWith(tenantId, service, resource, [permission.READ], jwt))
    })

    it('should return hasPermissions result from canUpdate', async () => {
        const hasPermissionsStub = sinon.stub(jwtRbac, 'hasPermissions').returns(true)

        const result = jwtRbac.canUpdate(tenantId, service, resource, jwt)

        jwtRbac.hasPermissions.restore()

        expect(result).to.be.true
        expect(hasPermissionsStub.calledOnce)
        expect(hasPermissionsStub.calledWith(tenantId, service, resource, [permission.UPDATE], jwt))
    })

    it('should return hasPermissions result from canDelete', async () => {
        const hasPermissionsStub = sinon.stub(jwtRbac, 'hasPermissions').returns(true)

        const result = jwtRbac.canDelete(tenantId, service, resource, jwt)

        jwtRbac.hasPermissions.restore()

        expect(result).to.be.true
        expect(hasPermissionsStub.calledOnce)
        expect(hasPermissionsStub.calledWith(tenantId, service, resource, [permission.DELETE], jwt))
    })

    it('should throw an error on hasPermissions with no permissions', async () => {
        expect(() => jwtRbac.hasPermissions(tenantId, service, resource, null, jwt)).to.throw(Error)
        expect(() => jwtRbac.hasPermissions(tenantId, service, resource, undefined, jwt)).to.throw(Error)
        expect(() => jwtRbac.hasPermissions(tenantId, service, resource, [], jwt)).to.throw(Error)
    })

    it('should return false if permissions are not in the jwt', async () => {
        const result = jwtRbac.hasPermissions(tenantId, service, resource, [permission.ALL], jwt)
        expect(result).to.be.false
    })

    it('should return false if tenant is not in the jwt', async () => {
        const jwt = { permissions: {} }
        const result = jwtRbac.hasPermissions(tenantId, service, resource, [permission.ALL], jwt)
        expect(result).to.be.false
    })

    it('should verify user create permissions', async () => {
        const jwt = {
            permissions: {
                TENANT_ID: {
                    SERVICE: {
                        RESOURCE: permission.CREATE
                    }
                }
            }
        }

        const resultNone = jwtRbac.hasPermissions(tenantId, service, resource, [permission.NONE], jwt)
        const resultCreate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE], jwt)
        const resultRead = jwtRbac.hasPermissions(tenantId, service, resource, [permission.READ], jwt)
        const resultUpdate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.UPDATE], jwt)
        const resultDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.DELETE], jwt)
        const resultCreateDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE, permission.DELETE], jwt)
        const resultAll = jwtRbac.hasPermissions(tenantId, service, resource, [permission.ALL], jwt)

        expect(resultNone).to.be.true
        expect(resultCreate).to.be.true
        expect(resultRead).to.be.false
        expect(resultUpdate).to.be.false
        expect(resultDelete).to.be.false
        expect(resultCreateDelete).to.be.false
        expect(resultAll).to.be.false
    })

    it('should verify user read permissions', async () => {
        const jwt = {
            permissions: {
                TENANT_ID: {
                    SERVICE: {
                        RESOURCE: permission.READ
                    }
                }
            }
        }

        const resultNone = jwtRbac.hasPermissions(tenantId, service, resource, [permission.NONE], jwt)
        const resultCreate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE], jwt)
        const resultRead = jwtRbac.hasPermissions(tenantId, service, resource, [permission.READ], jwt)
        const resultUpdate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.UPDATE], jwt)
        const resultDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.DELETE], jwt)
        const resultCreateDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE, permission.DELETE], jwt)
        const resultAll = jwtRbac.hasPermissions(tenantId, service, resource, [permission.ALL], jwt)

        expect(resultNone).to.be.true
        expect(resultCreate).to.be.false
        expect(resultRead).to.be.true
        expect(resultUpdate).to.be.false
        expect(resultDelete).to.be.false
        expect(resultCreateDelete).to.be.false
        expect(resultAll).to.be.false
    })

    it('should verify user update permissions', async () => {
        const jwt = {
            permissions: {
                TENANT_ID: {
                    SERVICE: {
                        RESOURCE: permission.UPDATE
                    }
                }
            }
        }

        const resultNone = jwtRbac.hasPermissions(tenantId, service, resource, [permission.NONE], jwt)
        const resultCreate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE], jwt)
        const resultRead = jwtRbac.hasPermissions(tenantId, service, resource, [permission.READ], jwt)
        const resultUpdate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.UPDATE], jwt)
        const resultDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.DELETE], jwt)
        const resultCreateDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE, permission.DELETE], jwt)
        const resultAll = jwtRbac.hasPermissions(tenantId, service, resource, [permission.ALL], jwt)

        expect(resultNone).to.be.true
        expect(resultCreate).to.be.false
        expect(resultRead).to.be.false
        expect(resultUpdate).to.be.true
        expect(resultDelete).to.be.false
        expect(resultCreateDelete).to.be.false
        expect(resultAll).to.be.false
    })

    it('should verify user delete permissions', async () => {
        const jwt = {
            permissions: {
                TENANT_ID: {
                    SERVICE: {
                        RESOURCE: permission.DELETE
                    }
                }
            }
        }

        const resultNone = jwtRbac.hasPermissions(tenantId, service, resource, [permission.NONE], jwt)
        const resultCreate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE], jwt)
        const resultRead = jwtRbac.hasPermissions(tenantId, service, resource, [permission.READ], jwt)
        const resultUpdate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.UPDATE], jwt)
        const resultDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.DELETE], jwt)
        const resultCreateDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE, permission.DELETE], jwt)
        const resultAll = jwtRbac.hasPermissions(tenantId, service, resource, [permission.ALL], jwt)

        expect(resultNone).to.be.true
        expect(resultCreate).to.be.false
        expect(resultRead).to.be.false
        expect(resultUpdate).to.be.false
        expect(resultDelete).to.be.true
        expect(resultCreateDelete).to.be.false
        expect(resultAll).to.be.false
    })

    it('should verify user create and delete permissions', async () => {
        const jwt = {
            permissions: {
                TENANT_ID: {
                    SERVICE: {
                        RESOURCE: permission.CREATE | permission.DELETE
                    }
                }
            }
        }

        const resultNone = jwtRbac.hasPermissions(tenantId, service, resource, [permission.NONE], jwt)
        const resultCreate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE], jwt)
        const resultRead = jwtRbac.hasPermissions(tenantId, service, resource, [permission.READ], jwt)
        const resultUpdate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.UPDATE], jwt)
        const resultDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.DELETE], jwt)
        const resultCreateDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE, permission.DELETE], jwt)
        const resultAll = jwtRbac.hasPermissions(tenantId, service, resource, [permission.ALL], jwt)

        expect(resultNone).to.be.true
        expect(resultCreate).to.be.true
        expect(resultRead).to.be.false
        expect(resultUpdate).to.be.false
        expect(resultDelete).to.be.true
        expect(resultCreateDelete).to.be.true
        expect(resultAll).to.be.false
    })

    it('should verify all service and resource create and delete permissions', async () => {
        const jwt = {
            permissions: {
                TENANT_ID: {
                    all: {
                        all: permission.CREATE | permission.DELETE
                    }
                }
            }
        }

        const resultNone = jwtRbac.hasPermissions(tenantId, service, resource, [permission.NONE], jwt)
        const resultCreate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE], jwt)
        const resultRead = jwtRbac.hasPermissions(tenantId, service, resource, [permission.READ], jwt)
        const resultUpdate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.UPDATE], jwt)
        const resultDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.DELETE], jwt)
        const resultCreateDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE, permission.DELETE], jwt)
        const resultAll = jwtRbac.hasPermissions(tenantId, service, resource, [permission.ALL], jwt)

        expect(resultNone).to.be.true
        expect(resultCreate).to.be.true
        expect(resultRead).to.be.false
        expect(resultUpdate).to.be.false
        expect(resultDelete).to.be.true
        expect(resultCreateDelete).to.be.true
        expect(resultAll).to.be.false
    })

    it('should verify mixed all and service and resource create and delete permissions', async () => {
        const jwt = {
            permissions: {
                TENANT_ID: {
                    all: {
                        all: permission.CREATE
                    },
                    SERVICE: {
                        RESOURCE: permission.DELETE
                    }
                }
            }
        }

        const resultNone = jwtRbac.hasPermissions(tenantId, service, resource, [permission.NONE], jwt)
        const resultCreate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE], jwt)
        const resultRead = jwtRbac.hasPermissions(tenantId, service, resource, [permission.READ], jwt)
        const resultUpdate = jwtRbac.hasPermissions(tenantId, service, resource, [permission.UPDATE], jwt)
        const resultDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.DELETE], jwt)
        const resultCreateDelete = jwtRbac.hasPermissions(tenantId, service, resource, [permission.CREATE, permission.DELETE], jwt)
        const resultAll = jwtRbac.hasPermissions(tenantId, service, resource, [permission.ALL], jwt)

        expect(resultNone).to.be.true
        expect(resultCreate).to.be.true
        expect(resultRead).to.be.false
        expect(resultUpdate).to.be.false
        expect(resultDelete).to.be.true
        expect(resultCreateDelete).to.be.true
        expect(resultAll).to.be.false
    })

    it('should verify user is master', async () => {
        const result = jwtRbac.isMaster({master: true})

        expect(result).to.be.true
    })

    it('should verify user is not master if absent', async () => {
        const result = jwtRbac.isMaster({permissions: {}})

        expect(result).to.be.false
    })

    it('should verify user is not master if false', async () => {
        const result = jwtRbac.isMaster({master: false})

        expect(result).to.be.false
    })

    it('should verify master has every permission', async () => {
        const jwt = {
            master: true
        }
        const otherTenantId = "OTHER_TENANT_NOT_IN_JWT"

        const resultNone = jwtRbac.hasPermissions(otherTenantId, service, resource, [permission.NONE], jwt)
        const resultCreate = jwtRbac.hasPermissions(otherTenantId, service, resource, [permission.CREATE], jwt)
        const resultRead = jwtRbac.hasPermissions(otherTenantId, service, resource, [permission.READ], jwt)
        const resultUpdate = jwtRbac.hasPermissions(otherTenantId, service, resource, [permission.UPDATE], jwt)
        const resultDelete = jwtRbac.hasPermissions(otherTenantId, service, resource, [permission.DELETE], jwt)
        const resultCreateDelete = jwtRbac.hasPermissions(otherTenantId, service, resource, [permission.CREATE, permission.DELETE], jwt)
        const resultAll = jwtRbac.hasPermissions(otherTenantId, service, resource, [permission.ALL], jwt)

        expect(resultNone).to.be.true
        expect(resultCreate).to.be.true
        expect(resultRead).to.be.true
        expect(resultUpdate).to.be.true
        expect(resultDelete).to.be.true
        expect(resultCreateDelete).to.be.true
        expect(resultAll).to.be.true
    })
})
