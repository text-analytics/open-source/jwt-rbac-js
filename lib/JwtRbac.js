'use strict';

var rp = require('request-promise-native')
var jsonwebtoken = require('jsonwebtoken')
var jwt = require('express-jwt')

var permission = {
    CREATE: 0b1000,
    READ: 0b0100,
    UPDATE: 0b0010,
    DELETE: 0b0001,
    NONE: 0b0000,
    ALL: 0b1111,
}

/**
 * @param {string} padlockUrl
 * @param {string} audience
 * @param {string} [publicKey=null] fetched from padlock if omitted
 * @param {import('events').EventEmitter} [eventEmitter=null]
 */
exports = module.exports = function (padlockUrl, audience, publicKey = null, eventEmitter = null) {
    this.tokenIssuer = 'convergysiq'
    this.tokenAlgorithm = 'RS256'
    this.padlockUrl = padlockUrl
    this.audience = audience
    if (publicKey) {
        this.publicKey = publicKey
        if (eventEmitter) eventEmitter.emit('padlock-public-key-obtained')
    } else rp(`${padlockUrl}/v0/tokens/public-key`).then((body) => {
        this.publicKey = JSON.parse(body)
        if (eventEmitter) eventEmitter.emit('padlock-public-key-obtained')
    })

    /**
     * @param {string} serviceName
     * @param {object[]} resources
     * @param {string} resources[].name
     * @param {string} resources[].description
     */
    this.registerResources = (serviceName, resources) => {
        const resourceObjects = resources.map(resource => ({
            name: resource.name,
            description: resource.description,
            service: serviceName
        }))

        rp({
            method: 'POST',
            uri: `${padlockUrl}/v0/resources`,
            body: resourceObjects,
            json: true
        }).then(body => {
            console.log('registered resources with padlock')
        }).catch(err => {
            console.error('failed to register resources with padlock')
        })
    }

    /**
     * @param {string} tenantId 
     * @param {string} service
     * @param {string} resource
     * @param {object} decodedJwt
     * 
     * @returns {boolean}
     */
    this.canCreate = (tenantId, service, resource, decodedJwt) => {
        return this.hasPermissions(tenantId, service, resource, [permission.CREATE], decodedJwt)
    }

    /**
     * @param {string} tenantId 
     * @param {string} service
     * @param {string} resourcetruthy
     * @param {object} decodedJwt
     * 
     * @returns {boolean}
     */
    this.canRead = (tenantId, service, resource, decodedJwt) => {
        return this.hasPermissions(tenantId, service, resource, [permission.READ], decodedJwt)
    }

    /**
     * @param {string} tenantId 
     * @param {string} service
     * @param {string} resource
     * @param {object} decodedJwt
     * 
     * @returns {boolean}
     */
    this.canUpdate = (tenantId, service, resource, decodedJwt) => {
        return this.hasPermissions(tenantId, service, resource, [permission.UPDATE], decodedJwt)
    }

    /**
     * @param {string} tenantId 
     * @param {string} service
     * @param {string} resource
     * @param {object} decodedJwt
     * 
     * @returns {boolean}
     */
    this.canDelete = (tenantId, service, resource, decodedJwt) => {
        return this.hasPermissions(tenantId, service, resource, [permission.DELETE], decodedJwt)
    }

    /**
     * @param {object} decodedJwt
     * 
     * @returns {boolean}
     */
    this.isMaster = (decodedJwt) => {
        // !! is used to coerce undefined into false
        return decodedJwt && !!decodedJwt.master
    }

    /**
     * @param {string} tenantId 
     * @param {string} service
     * @param {string} resource
     * @param {permission[]} permissions 
     * @param {object} decodedJwt
     * 
     * @returns {boolean}
     */
    this.hasPermissions = (tenantId, service, resource, permissions, decodedJwt) => {
        // Master can do all things
        if (this.isMaster(decodedJwt)) {
            return true
        }

        if (permissions == null || permissions.length === 0) {
            throw new Error('permissions are required to verify permissions?!')
        }

        let tenant
        try {
            tenant = decodedJwt && decodedJwt.permissions[tenantId]
            if (!tenant) throw TypeError('tenant is undefined')
        } catch (TypeError) {
            return false
        }

        let requestedPermissions = permission.NONE
        for (let permission of permissions) {
            requestedPermissions |= permission
        }

        let actualPermissions = permission.NONE
        try {
            const resourcePermissions = tenant[service][resource]
            if (resourcePermissions) actualPermissions |= resourcePermissions
        } catch (TypeError) {
            // Do Nothing: permissions aren't present
        }

        // check special all service and resource
        try {
            const resourcePermissions = tenant.all.all
            if (resourcePermissions) actualPermissions |= resourcePermissions
        } catch (TypeError) {
            // Do Nothing: permissions aren't present
        }

        return (requestedPermissions & actualPermissions) === requestedPermissions
    }

    /**
     * @param {String} token JWT
     * 
     * @returns {Promise} promise that resolves to the decoded JWT object
     */
    this.decodeJWT = (token) => {
        return new Promise((resolve, reject) => {
            jsonwebtoken.verify(token, this.publicKey, {
                algorithms: [this.tokenAlgorithm],
                audience: this.audience,
                issuer: this.tokenIssuer
            }, (err, decoded) => {
                if (err) reject(err)
                else resolve(decoded)
            })
        })
    }

    /**
     * validate and decode jwt from header and place in req.jwt for use by all endpoints.
     */
    this.requiredOnAll = () => {
        return this.requiredExceptOn([])
    }

    /**
     * validate and decode jwt from header and place in req.jwt for use by endpoints with exception.
     * 
     * @param {object[]} exclude the paths to exclude from jwt being required and parsed (module: express-unless)
     */
    this.requiredExceptOn = (exclude = []) => {
        return jwt({
            requestProperty: 'jwt',
            secret: this.publicKey,
            algorithms: [this.tokenAlgorithm],
            audience: this.audience,
            issuer: this.tokenIssuer
        }).unless({ path: exclude })
    }

    this.jwtOptional = () => {
        return jwt({
            requestProperty: 'jwt',
            secret: this.publicKey,
            algorithms: [this.tokenAlgorithm],
            audience: this.audience,
            issuer: this.tokenIssuer,
            credentialsRequired: false
        })
    }
}

exports.permission = permission