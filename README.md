# JWT RBAC JS

[![pipeline status](https://gitlab.com/text-analytics/open-source/jwt-rbac-js/badges/master/pipeline.svg)](https://gitlab.com/text-analytics/open-source/jwt-rbac-js/commits/master)

[![coverage report](https://gitlab.com/text-analytics/open-source/jwt-rbac-js/badges/master/coverage.svg)](https://gitlab.com/text-analytics/open-source/jwt-rbac-js/commits/master)

## Install
```bash
yarn add https://gitlab.com/text-analytics/open-source/jwt-rbac-js.git
```

## Example

### Required on all
```javascript
import express from 'express'
import { default as JwtRbac, permission } from 'jwt-rbac'

const padlockUrl = 'https://padlock-url.com'
const audience = 'production'
const jwtRbac = new JwtRbac(padlockUrl, audience)

const app = express()

app.use(jwtRbac.requiredOnAll())

app.get('/private', (req, res) => {
    console.log(`decoded JWT: ${req.jwt}`)

    const hasPermission = jwtRbac.canRead('TENANT_ID', 'SERVICE', 'RESOURCE', req.jwt)
    if (hasPermission) {
        console.log('can read')
    } else console.log('cannot read')
    
    res.json('this message is returned if a JWT was provided')
})

app.listen(3000, () => console.log('Example app listening at http://localhost:3000'))
```

### Required with exception
```javascript
import express from 'express'
import { default as JwtRbac, permission } from 'jwt-rbac'

const padlockUrl = 'https://padlock-url.com'
const audience = 'production'
const jwtRbac = new JwtRbac(padlockUrl, audience)

const app = express()

// inputs paths parsed by node module: express-unless
app.use(jwtRbac.requiredExceptOn([
    '/public'
]))

app.get('/public', (req, res) => {
    res.json('this can be called without a JWT')
})

app.get('/private', (req, res) => {
    console.log(`decoded JWT: ${req.jwt}`)

    const hasPermission = jwtRbac.canRead('TENANT_ID', 'SERVICE', 'RESOURCE', req.jwt)
    if (hasPermission) {
        console.log('can read')
    } else console.log('cannot read')
    
    res.json('this message is returned if a JWT was provided')
})

app.listen(3000, () => console.log('Example app listening at http://localhost:3000'))
```